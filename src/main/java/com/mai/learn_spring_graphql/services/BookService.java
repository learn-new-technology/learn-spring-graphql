package com.mai.learn_spring_graphql.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mai.learn_spring_graphql.exceptions.BadRequestException;
import com.mai.learn_spring_graphql.models.entities.BookEntity;
import com.mai.learn_spring_graphql.repositories.AuthorRepository;
import com.mai.learn_spring_graphql.repositories.BookRepository;

@Service
@Transactional
public class BookService {

    private final BookRepository bookRepository;
    
    // private final AuthorRepository authorRepository;

    public BookService(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        // this.authorRepository = authorRepository;
    }

    public List<BookEntity> findBooks() {
        return bookRepository.findAll();
    }

    public BookEntity findBookByid(String id) {
        return bookRepository.findById(id).orElse(null);
    }

    public BookEntity findBookByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    public BookEntity saveBook(BookEntity bookEntity) {
        BookEntity book = new BookEntity();
        List<String> errorsList = new ArrayList<>();

        if (bookEntity.getId() != null) {
            book.setId(bookEntity.getId());
        }

        if (bookEntity.getTitle().isEmpty()) {
            errorsList.add("Book name is can not be blank");
        }
        book.setTitle(bookEntity.getTitle());

        if (bookEntity.getIsbn().isEmpty()) {
            errorsList.add("Book isbn is can not be blank");
        }
        book.setIsbn(bookEntity.getIsbn());

        if (bookEntity.getPageCount() < 5) {
            errorsList.add("Book page count is can not be less than 5 pages");
        }
        book.setPageCount(bookEntity.getPageCount());


        if (!errorsList.isEmpty()) {
            StringBuilder errorMessage = new StringBuilder();

            for (String error : errorsList) {
                errorMessage.append(error).append(" | ");
            }

            throw new BadRequestException(errorMessage.toString());
        }
        
        return bookRepository.save(book);
    }

    public boolean deleteBookById(String id) {
        BookEntity book = bookRepository.findById(id).orElse(null);

        if (book == null)
            return false;
        
        bookRepository.delete(book);

        return true;
    }
}