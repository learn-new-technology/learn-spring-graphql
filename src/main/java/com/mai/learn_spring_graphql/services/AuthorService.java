package com.mai.learn_spring_graphql.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mai.learn_spring_graphql.exceptions.BadRequestException;
import com.mai.learn_spring_graphql.models.entities.AuthorEntity;
import com.mai.learn_spring_graphql.repositories.AuthorRepository;
import com.mai.learn_spring_graphql.utils.Validation;

@Service
@Transactional
public class AuthorService {

    private final Validation validation;

    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository, Validation validation) {
        this.validation = validation;
        this.authorRepository = authorRepository;
    }

    public List<AuthorEntity> findAuthors() {
        return authorRepository.findAll();
    }

    public AuthorEntity findAuthorById(String id) {
        return authorRepository.findById(id).orElse(null);
    }

    public AuthorEntity findAuthorByEmail(String email) {
        return authorRepository.findByEmail(email);
    }

    public AuthorEntity saveAuthor(AuthorEntity authorEntity) {
        AuthorEntity author = new AuthorEntity();
        List<String> errorsList = new ArrayList<>();

        if (authorEntity.getId() != null) {
            author.setId(authorEntity.getId());
        }

        if (!validation.isValidEmail(authorEntity.getEmail())) {
            errorsList.add("Author email is incorrect");
        }
        author.setEmail(authorEntity.getEmail());


        if (!errorsList.isEmpty()) {
            StringBuilder errorMessage = new StringBuilder();

            for (String error : errorsList) {
                errorMessage.append(error).append(" | ");
            }

            throw new BadRequestException(errorMessage.toString());
        }
        
        return authorRepository.save(authorEntity);
    }

    public boolean deleteAuthorById(String id) {
        AuthorEntity author = authorRepository.findById(id).orElse(null);

        if (author == null)
            return false;
        
        authorRepository.delete(author);

        return true;
    }
}
