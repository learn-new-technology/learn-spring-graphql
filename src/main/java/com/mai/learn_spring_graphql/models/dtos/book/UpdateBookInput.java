package com.mai.learn_spring_graphql.models.dtos.book;

import com.mai.learn_spring_graphql.models.entities.BookEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateBookInput {

    private String id;

    private String title;

    private String isbn;

    private int pageCount;

    public BookEntity toBookEntity() {
        BookEntity bookEntity = new BookEntity();

        bookEntity.setId(id);
        bookEntity.setTitle(title);
        bookEntity.setIsbn(isbn);
        bookEntity.setPageCount(pageCount);

        return bookEntity;
    }
}