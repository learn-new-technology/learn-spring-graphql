package com.mai.learn_spring_graphql.models.dtos.book;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mai.learn_spring_graphql.models.dtos.author.Author;
import com.mai.learn_spring_graphql.models.entities.BookEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    
    private String id;

    private String title;

    private String isbn;

    private int pageCount;

    private List<Author> authors = new ArrayList<>();

    public Book(BookEntity bookEntity) {
        this.id = bookEntity.getId();
        this.title = bookEntity.getTitle();
        this.isbn = bookEntity.getIsbn();
        this.pageCount = bookEntity.getPageCount();
        this.authors = bookEntity.getAuthors().stream().map(author -> new Author(author)).collect(Collectors.toList());
    }
}