package com.mai.learn_spring_graphql.models.dtos.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SuccessPayload {
    
    private int status;
    
    private String message;
}
