package com.mai.learn_spring_graphql.models.dtos.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookSuccessPayload {
    
    private int status;
    
    private Object book;
}
