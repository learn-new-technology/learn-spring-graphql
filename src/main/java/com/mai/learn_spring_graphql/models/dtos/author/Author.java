package com.mai.learn_spring_graphql.models.dtos.author;

import com.mai.learn_spring_graphql.models.entities.AuthorEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {

    private String id;

    private String email;

    private String lastName;

    private String firstName;
    
    public Author(AuthorEntity authorEntity) {
        this.id = authorEntity.getId();
        this.email = authorEntity.getEmail();
        this.lastName = authorEntity.getLastName();
        this.firstName = authorEntity.getFirstName();
    }
}