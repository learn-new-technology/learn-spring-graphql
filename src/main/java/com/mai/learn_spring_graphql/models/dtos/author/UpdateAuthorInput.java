package com.mai.learn_spring_graphql.models.dtos.author;

import com.mai.learn_spring_graphql.models.entities.AuthorEntity;

import lombok.Data;

@Data
public class UpdateAuthorInput {

    private String id;

    private String email;

    private String lastName;

    private String firstName;

    public AuthorEntity toAuthorEntity() {
        AuthorEntity authorEntity = new AuthorEntity();

        authorEntity.setId(id);
        authorEntity.setEmail(email.toLowerCase());
        authorEntity.setLastName(lastName.toUpperCase());
        authorEntity.setFirstName(firstName.toUpperCase());

        return authorEntity;
    }
}
