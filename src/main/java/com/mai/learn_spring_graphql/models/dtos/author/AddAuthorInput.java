package com.mai.learn_spring_graphql.models.dtos.author;

import com.mai.learn_spring_graphql.models.entities.AuthorEntity;

import lombok.Data;

@Data
public class AddAuthorInput {

    private String email;

    private String lastName;

    private String firstName;

    public AuthorEntity toAuthorEntity() {
        AuthorEntity authorEntity = new AuthorEntity();

        authorEntity.setEmail(email.toLowerCase());
        authorEntity.setLastName(lastName);
        authorEntity.setFirstName(firstName);

        return authorEntity;
    }
}
