package com.mai.learn_spring_graphql.models.dtos.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorSuccessPayload {
    
    private int status;
    
    private Object author;
}
