package com.mai.learn_spring_graphql.models.dtos.book;

import com.mai.learn_spring_graphql.models.entities.BookEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddBookInput {

    private String title;

    private String isbn;

    private int pageCount;

    public BookEntity toBookEntity() {
        BookEntity bookEntity = new BookEntity();

        bookEntity.setTitle(title);
        bookEntity.setIsbn(isbn);
        bookEntity.setPageCount(pageCount);

        return bookEntity;
    }
}
