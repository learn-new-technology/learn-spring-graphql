package com.mai.learn_spring_graphql.models.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.mai.learn_spring_graphql.models.entities.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "books")
@EqualsAndHashCode(callSuper = false)
public class BookEntity extends BaseEntity {
    
    @Column(name = "title", unique = true, nullable = false, length = 100)
    private String title;

    @Column(name = "isbn", nullable = false, length = 100)
    private String isbn;

    @Column(name = "page_count", nullable = false)
    private int pageCount;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<AuthorEntity> authors = new HashSet<>();
}