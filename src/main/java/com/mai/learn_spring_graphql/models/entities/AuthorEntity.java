package com.mai.learn_spring_graphql.models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.mai.learn_spring_graphql.models.entities.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "authors")
@EqualsAndHashCode(callSuper = false)
public class AuthorEntity extends BaseEntity {
    
    @Column(name = "email", unique = true, nullable = false, length = 100)
    private String email;

    @Column(name = "last_name", nullable = false, length = 100)
    private String lastName;

    @Column(name = "first_name", nullable = false, length = 100)
    private String firstName;

    public AuthorEntity(String id) {
        this.id = id;
    }
}
