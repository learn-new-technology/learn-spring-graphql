package com.mai.learn_spring_graphql.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import com.mai.learn_spring_graphql.exceptions.BadRequestException;
import com.mai.learn_spring_graphql.models.dtos.book.AddBookInput;
import com.mai.learn_spring_graphql.models.dtos.book.Book;
import com.mai.learn_spring_graphql.models.dtos.book.BookSuccessPayload;
import com.mai.learn_spring_graphql.models.dtos.payload.FailedPayload;
import com.mai.learn_spring_graphql.models.dtos.payload.SuccessPayload;
import com.mai.learn_spring_graphql.models.entities.BookEntity;
import com.mai.learn_spring_graphql.services.BookService;
import com.mai.learn_spring_graphql.utils.Translator;

@Controller
public class BookController {
    
    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @SchemaMapping(typeName = "Query",value = "books")
    public List<Book> authors() { 
        List<Book> bookList = new ArrayList<>();
        
        bookService.findBooks().forEach(bookEntity -> bookList.add(new Book(bookEntity)));;    
        
        return bookList;
    }

    @QueryMapping(name = "bookById")
    public Object bookById(@Argument String id) {
        BookEntity bookEntity = bookService.findBookByid(id);

        if (bookEntity == null) {
            return new FailedPayload(404, Translator.toLocale("errors.bookNotFound"));
        }

        return new BookSuccessPayload(200, new Book(bookEntity));
    }

    @MutationMapping(name = "addBook")
    public Object addBook(@Argument("input") AddBookInput book) {
        if (bookService.findBookByTitle(book.getTitle()) != null) {
            return new FailedPayload(409, Translator.toLocale("errors.bookTitleConflict"));
        }

        try {System.out.print("200 Book");
            BookEntity bookEntity = bookService.saveBook(book.toBookEntity());
            return new BookSuccessPayload(200, new Book(bookEntity));
        }
        catch(BadRequestException exception) {
            return new FailedPayload(400, exception.getMessage());
        }
    }

    @MutationMapping(name = "deleteBookById")
    public Object deleteBookById(@Argument String id) {
        if (!bookService.deleteBookById(id)) {
            return new FailedPayload(404, Translator.toLocale("errors.bookNotFound"));
        }

        return new SuccessPayload(204, Translator.toLocale("success.bookDeleted"));
    }
}
