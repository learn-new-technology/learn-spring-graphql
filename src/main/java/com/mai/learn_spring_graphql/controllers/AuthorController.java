package com.mai.learn_spring_graphql.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import com.mai.learn_spring_graphql.models.dtos.author.Author;
import com.mai.learn_spring_graphql.models.dtos.author.AuthorSuccessPayload;
import com.mai.learn_spring_graphql.exceptions.BadRequestException;
import com.mai.learn_spring_graphql.models.dtos.author.AddAuthorInput;
import com.mai.learn_spring_graphql.models.dtos.payload.FailedPayload;
import com.mai.learn_spring_graphql.models.dtos.payload.SuccessPayload;
import com.mai.learn_spring_graphql.models.entities.AuthorEntity;
import com.mai.learn_spring_graphql.services.AuthorService;
import com.mai.learn_spring_graphql.utils.Translator;

@Controller
public class AuthorController {
    
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @SchemaMapping(typeName = "Query",value = "authors")
    public List<Author> authors() { 
        List<Author> authorList = new ArrayList<>();
        
        authorService.findAuthors().forEach(authorEntity -> authorList.add(new Author(authorEntity)));;    
        
        return authorList;
    }

    @QueryMapping(name = "authorById")
    public Object authorById(@Argument String id) {
        AuthorEntity authorEntity = authorService.findAuthorById(id);

        if (authorEntity == null) {
            return new FailedPayload(404, Translator.toLocale("errors.authorNotFound"));
        }

        return new AuthorSuccessPayload(200, new Author(authorEntity));
    }

    @MutationMapping(name = "addAuthor")
    public Object addAuthor(@Argument("input") AddAuthorInput authorInput) {
        if (authorService.findAuthorByEmail(authorInput.getEmail()) != null) {
            return new FailedPayload(409, Translator.toLocale("errors.authorEmailConflict"));
        }

        try {System.out.print("200 Author");
            AuthorEntity authorEntity = authorService.saveAuthor(authorInput.toAuthorEntity());
            return new AuthorSuccessPayload(200, new Author(authorEntity));
        }
        catch(BadRequestException exception) {
            return new FailedPayload(400, exception.getMessage());
        }
    }

    @MutationMapping(name = "deleteAuthorById")
    public Object deleteAuthorById(@Argument String id) {
        if (!authorService.deleteAuthorById(id)) {
            return new FailedPayload(404, Translator.toLocale("errors.authorNotFound"));
        }

        return new SuccessPayload(204, Translator.toLocale("success.authorDeleted"));
    }
}
