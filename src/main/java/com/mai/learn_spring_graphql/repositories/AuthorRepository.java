package com.mai.learn_spring_graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mai.learn_spring_graphql.models.entities.AuthorEntity;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, String> {
    AuthorEntity findByEmail(String email);
}