package com.mai.learn_spring_graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mai.learn_spring_graphql.models.entities.BookEntity;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, String> {
    BookEntity findByTitle(String title);
}
