package com.mai.learn_spring_graphql.configurations.datas;

import java.util.HashSet;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.mai.learn_spring_graphql.models.entities.AuthorEntity;
import com.mai.learn_spring_graphql.models.entities.BookEntity;
import com.mai.learn_spring_graphql.repositories.AuthorRepository;
import com.mai.learn_spring_graphql.repositories.BookRepository;

@Configuration
public class DataConfig {
    
    @Bean
    CommandLineRunner initDataBase(Environment enviroment, AuthorRepository authorRepository, BookRepository bookRepository) {
        return args -> {
            System.out.println("Start of Laoding Default Datas");
            if (authorRepository.findAll().isEmpty()) {
                authorRepository.save(new AuthorEntity("user@email.com", "USER", "Few Access Reserved"));
                authorRepository.save(new AuthorEntity("admin@email.com", "ADMIN", "All Access Reserved"));
            }

            if (bookRepository.findAll().isEmpty()) {
                Set<AuthorEntity> authors = new HashSet<>();
                AuthorEntity userAuthor = authorRepository.findByEmail("user@email.com");
                AuthorEntity adminAuthor = authorRepository.findByEmail("admin@email.com");

                authors.add(userAuthor);
                bookRepository.save(new BookEntity("Java", "9782765409120", 500, authors));
                authors.add(adminAuthor);
                bookRepository.save(new BookEntity("Python", "9782765409121", 250, authors));
            }
            System.out.println("End of Laoding Default Datas \n");

            System.out.println("The application name is " + enviroment.getProperty("spring.application.name"));
        };
    }
}